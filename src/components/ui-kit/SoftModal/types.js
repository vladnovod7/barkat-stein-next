import Revit from './content/Revit'
import AutoCAD from './content/AutoCAD'
import AutodeskAecCollection from './content/AutodeskAecCollection'
import Navisworks from './content/Navisworks'
import Civil3D from './content/Civil3D'
import ThreeDExperience from './content/3DExperience'
import DraftSide from './content/DraftSide'
import Simulia from './content/Simulia'
import Delmia from './content/Delmia'
import Enovia from './content/Enovia'
import Catia from './content/Catia'
import GeoMedia from './content/GeoMedia'
import ErdasImagine from './content/ErdasImagine'
import ImagineStation from './content/ImagineStation'
import ErdasApollo from './content/ErdasApollo'
import MAppEnterprise from './content/MAppEnterprise'
import Luciad from './content/Luciad'
import MicroStation from './content/MicroStation'
import Plaxis from './content/Plaxis'
import Staad from './content/Staad'
import SketchUpPro from './content/SketchUpPro'
import ExtensionWarehouse from './content/ExtensionWarehouse'
import SketchUpForInternet from './content/SketchUpForInternet'
import AllPlanArchitecture from './content/AllPlanArchitecture'

export default {
  REVIT: {
    title: 'Autodesk Revit',
    content: Revit
  },
  AUTOCAD: {
    title: 'AutoCAD',
    content: AutoCAD
  },
  AUTODESK_AEC_COLLECTION: {
    title: 'Autodesk AEC Collection',
    content: AutodeskAecCollection
  },
  NAVISWORKS: {
    title: 'Autodesk Navisworks',
    content: Navisworks
  },
  CIVIL_3D: {
    title: 'Autodesk Civil 3D',
    content: Civil3D
  },
  '3DExperience' : {
    title: '3DExperience',
    content: ThreeDExperience
  },
  DraftSide: {
    title: 'DraftSide',
    content: DraftSide
  },
  Catia: {
    title: 'Catia',
    content: Catia
  },
  Enovia: {
    title: 'Enovia',
    content: Enovia
  },
  Delmia: {
    title: 'Delmia',
    content: Delmia
  },
  Simulia: {
    title: 'Simulia',
    content: Simulia
  },
  GeoMedia : {
    title: 'GeoMedia',
    content: GeoMedia
  },
  ErdasImagine: {
    title: 'Erdas Imagine',
    content: ErdasImagine
  },
  ImagineStation: {
    title: 'Imagine Station',
    content: ImagineStation
  },
  ErdasApollo: {
    title: 'Erdas Apollo',
    content: ErdasApollo
  },
  MAppEnterprise: {
    title: 'M.App Enterprise',
    content: MAppEnterprise
  },
  Luciad: {
    title: 'Luciad',
    content: Luciad
  },
  MicroStation: {
    title: 'MicroStation',
    content: MicroStation
  },
  Plaxis: {
    title: 'PLAXIS',
    content: Plaxis
  },
  Staad: {
    title: 'STAAD',
    content: Staad
  },
  SketchUpPro: {
    title: 'SketchUp Pro',
    content: SketchUpPro
  },
  ExtensionWarehouse: {
    title: 'Extension Warehouse',
    content: ExtensionWarehouse
  },
  SketchUpForInternet: {
    title: 'SketchupUp для интернета',
    content: SketchUpForInternet
  },
  AllPlanArchitecture: {
    title: 'Allplan Architecture',
    content: AllPlanArchitecture
  }
}